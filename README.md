# InstanceOfSingletonType

No more fuss with constructing an instance of a complicated singleton
type that stores lots of data in the type domain.

## Usage examples:

```
julia> instance_of_singleton_type(Tuple{Nothing, Missing, Val{3}})
(nothing, missing, Val{3}())
```

```
julia> instance_of_singleton_type(Val{Val(Val(Val(7)))})
Val{Val{Val{Val{7}()}()}()}()
```

This fails, as expected, because `3` isn't a type:
```
julia> instance_of_singleton_type(3)
ERROR: MethodError: no method matching instance_of_singleton_type(::Int64)
```

This fails, as expected, because `Int` isn't a singleton type:
```
julia> instance_of_singleton_type(Int)
ERROR: Int64 isn't a singleton type
```

### An interesting usecase

Success, even though the type's inner constructor seems to disallow
the creation of an instance. This succeeds because
`instance_of_singleton_type` relies on uninitialized construction with
`new`, instead of on a given type's constructor. See
[here](https://docs.julialang.org/en/v1/manual/constructors/#Incomplete-Initialization).
```
julia> struct InstancesDisallowed
         InstancesDisallowed() = error("no can do")
       end

julia> instance_of_singleton_type(InstancesDisallowed)
InstancesDisallowed()

julia> InstancesDisallowed()
ERROR: no can do
```
