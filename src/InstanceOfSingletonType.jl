# Copyright © 2023 Neven Sajko. All rights reserved. Licensed under the
# MIT license, see the LICENSE file.

module InstanceOfSingletonType

export instance_of_singleton_type

"""
https://docs.julialang.org/en/v1/manual/constructors/#Incomplete-Initialization
"""
struct Uninitialized{T <: Any}
  t::T

  Uninitialized{T}() where {T <: Any} = new{T}()
end

"""
Usage examples:

```
instance_of_singleton_type(Tuple{Nothing, Missing, Val{3}})
```

```
instance_of_singleton_type(Val{Val(Val(Val(7)))})
```

This fails, as expected, because `3` isn't a type:
```
instance_of_singleton_type(3)
```

This fails, as expected, because `Int` isn't a singleton type:
```
instance_of_singleton_type(Int)
```
"""
function instance_of_singleton_type(::Type{T}) where {T <: Any}
  Base.issingletontype(T) || error("$T isn't a singleton type")

  # As pointed out by Mason Protter, using `T.instance` should have the
  # same effect as the below line. I don't know whether the `instance`
  # field is documented or supported, though; it seems like an
  # implementation detail.
  Uninitialized{T}().t
end

end
